(require 'ox-publish)

(setq org-publish-project-alist
      '(("site-org"
         :recursive                       t
         :base-directory                  "./content/org-files"
         :publishing-directory            "./public"
	 :exclude                         "setup.org"
         :publishing-function             org-html-publish-to-html
	 :with-author                     nil
         :with-creator                    t
         :with-toc                        nil
         :section-numbers                 nil
         :time-stamp-file                 nil
	 :with-smart-quotes               t
	 :html-container                  "section"
	 ;;:html-head-include-default-style nil
	 ;;:html-head-include-scripts       nil
	 :html-doctype                    "html5"
	 :html-html5-fancy                t
	 :html-preamble                   t
	 :html-validation-link            nil)
	("site-static"
	 :base-directory       "./content/static"
	 :base-extension       "css\\|png\\|pdf"
       	 :publishing-directory "./public"
       	 :publishing-function  org-publish-attachment
       	 :recursive            t)
	("site-css"
	 :base-directory       "./content/style"
	 :base-extension       "css"
       	 :publishing-directory "./public/style"
       	 :publishing-function  org-publish-attachment)
	("site" :components ("site-org" "site-static" "site-css"))))


;;(provide 'publish)

;;(org-publish-all t)
(org-publish "site" t)
